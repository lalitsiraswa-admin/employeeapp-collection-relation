package com.employeeapp.exceptions;

import com.employeeapp.model.ApiErrors;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LocalDateTime localDateTime = LocalDateTime.now();
        String message = ex.getMessage();
        List<String> details = Arrays.asList("API Called Using Wrong Method!", ex.getMessage());
        ApiErrors errors = new ApiErrors(localDateTime, status, status.value(), message, details);
        return ResponseEntity.status(status).headers(headers).body(errors);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LocalDateTime localDateTime = LocalDateTime.now();
        String message = ex.getMessage();
        List<String> details = Arrays.asList("API Called Using Wrong Media Type!", ex.getMessage());
        ApiErrors errors = new ApiErrors(localDateTime, status, status.value(), message, details);
        return ResponseEntity.status(status).headers(headers).body(errors);
    }

    @Override
    protected ResponseEntity<Object> handleMissingPathVariable(MissingPathVariableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LocalDateTime localDateTime = LocalDateTime.now();
        String message = ex.getMessage();
        List<String> details = Arrays.asList("API Called With The Missing Path Variable!", ex.getMessage());
        ApiErrors errors = new ApiErrors(localDateTime, status, status.value(), message, details);
        return ResponseEntity.status(status).headers(headers).body(errors);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LocalDateTime localDateTime = LocalDateTime.now();
        String message = ex.getMessage();
        List<String> details = Arrays.asList("API Called With The Missing Request Parameter!", ex.getMessage());
        ApiErrors errors = new ApiErrors(localDateTime, status, status.value(), message, details);
        return ResponseEntity.status(status).headers(headers).body(errors);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(TypeMismatchException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LocalDateTime datetime = LocalDateTime.now();
        String message = ex.getMessage();
        List<String> details = Arrays.asList("Data Types Are Not Matching!", ex.getMessage());
        ApiErrors errors = new ApiErrors(datetime, status, status.value(), message, details);
        return ResponseEntity.status(status).headers(headers).body(errors);
    }
    @ExceptionHandler(EmployeeNotFoundException.class)
    public ResponseEntity<Object> handleEmployeeNotFoundException(EmployeeNotFoundException e){
        LocalDateTime localDateTime = LocalDateTime.now();
        String message = e.getMessage();
        List<String> details = Arrays.asList("Employee Not Found!!", e.getMessage());
        ApiErrors errors = new ApiErrors(localDateTime, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), message, details);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
    }
    @ExceptionHandler(IdNotFoundException.class)
    public ResponseEntity<Object> handleIdNotFoundException(IdNotFoundException e){
        LocalDateTime localDateTime = LocalDateTime.now();
        String message = e.getMessage();
        List<String> details = Arrays.asList("Employee Not Found!!", e.getMessage());
        ApiErrors errors = new ApiErrors(localDateTime, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), message, details);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
    }
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> handleResourceNotFoundException(ResourceNotFoundException e){
        LocalDateTime localDateTime = LocalDateTime.now();
        String message = e.getMessage();
        List<String> details = Arrays.asList("Resource Not Found!!", e.getMessage());
        ApiErrors errors = new ApiErrors(localDateTime, HttpStatus.BAD_REQUEST, HttpStatus.BAD_REQUEST.value(), message, details);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errors);
    }

}
