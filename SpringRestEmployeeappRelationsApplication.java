package com.employeeapp;

import com.employeeapp.model.*;
import com.employeeapp.services.IEmployeeService;
import com.employeeapp.services.IResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class SpringRestEmployeeappRelationsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SpringRestEmployeeappRelationsApplication.class, args);
	}
	@Autowired
	IResourceService resourceService;
	@Autowired
	IEmployeeService employeeService;
	@Override
	public void run(String... args) throws Exception {
		Resource resource = new Resource(1, "Car", ResourceType.VEHICLE,Grade.DEVELOPER.getGradeName(), LocalDate.of(2021,10,22));
		resourceService.addResource(resource);
		Resource resource1 = new Resource(2, "Head Phones", ResourceType.ELECTRONICS, Grade.DEVELOPER.getGradeName(), LocalDate.of(2020,11,21));
		resourceService.addResource(resource1);
		List<Resource> resourceList = Arrays.asList(resource,resource1);
		System.out.println(resourceList);
		Address address = new Address("Bangalore","KAR",560068);
		Employee employee =
				new Employee("Jenifer",
						53,
						20000,
						"BU",
						"Developer",
						address,
						new String[]{"golf","music"}, resourceList);
		employeeService.addEmployee(employee);
//    System.out.println("By Hobbies");
//    employeeService.getByHobbies("golf").forEach(System.out::println);
//    System.out.println("by city");
//    employeeService.getByCity("Bangalore").forEach(System.out::println);
//    System.out.println("By Type");
//    employeeService.getByType("vehicles".toUpperCase()).forEach(System.out::println);
//    System.out.println("By D and type");
//    employeeService.getByDesignationAndType("Developer","electronics".toUpperCase()).forEach(System.out::println);
	}
}



