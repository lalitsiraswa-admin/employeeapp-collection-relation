package com.employeeapp.repository;

import com.employeeapp.exceptions.EmployeeNotFoundException;
import com.employeeapp.exceptions.ResourceNotFoundException;
import com.employeeapp.model.Employee;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IEmployeeRepository extends MongoRepository<Employee, Integer> {
    // derived
    List<Employee> findByDesignation(String designation, Sort sort);
    List<Employee> findByDepartment(String department);

    // address is not a primitive type, it is a class, so you need to go into
    // that class and find city(It is a primitive type variable)
    // here address is the name of the instance variable in the Employee class
    List<Employee> findByAddressCity(String city);

    // custom
    @Query("{'address.state':?0}")
    List<Employee> findByState(String state); // findByAddressState(String state);

    @Query("{'resources.resourceName':?0}")
    List<Employee> findByResourceName(String resourceName);
    @Query("{'resources.type':?0}")
    List<Employee> findByType(String type);
    @Query("{'resources.grade':?0}")
    List<Employee> findByGrade(String grade);

    @Query("{'resources.grade':?0, 'address.city':?1}")
//    @Query("{$and:[{'resources.grade':?0}, {'address.city':?1}]}")
    List<Employee> findByGradeAndCity(String grade, String city);
    @Query("{'hobbies':?0}")
    List<Employee> findByHobbies(String hobby);
    @Query("{'designation':?0, 'resources.type':?1}")
    List<Employee> findByDesignationAndType(String designation, String type);
}
