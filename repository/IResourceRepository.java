package com.employeeapp.repository;

import com.employeeapp.model.Resource;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IResourceRepository extends MongoRepository<Resource, Integer> {

}
