package com.employeeapp.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Address {
    private String city;
    private String state;
//    @Field(name="zip")
    private long zipcode;
}
