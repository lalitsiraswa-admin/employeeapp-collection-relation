package com.employeeapp.model;

public enum Grade {
    MANAGER("A", "Manager"),
    DEVELOPER("B", "Developer"),
    ADMIN("B", "Admin"),
    TESTERS("B", "Testers"),
    NETWORKING("C", "Networking"),
    INTERN("C", "Intern");
    private String gradeType;
    private String gradeName;

    Grade(String gradeType, String gradeName) {
        this.gradeType = gradeType;
        this.gradeName = gradeName;
    }

    public String getGradeType() {
        return gradeType;
    }

    public String getGradeName() {
        return gradeName;
    }
}
