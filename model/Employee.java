package com.employeeapp.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document(collection = "newemployee")
//@Document(collection = "employee")
public class Employee {
    private String name;
    @Id
    private Integer employeeId;
    private double salary;
    private String department;
    private String designation;
    private Address address; // embedded document
    private String[] hobbies;
    @DBRef // this gives the reference of the database, collection and _Id
    // This is a separate collection
    private List<Resource> resources;
}
