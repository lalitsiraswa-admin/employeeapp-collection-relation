package com.employeeapp.model;

public enum ResourceType {
    ELECTRONICS, VEHICLE, CLOTHING, STATIONARY, GIFTS
}
