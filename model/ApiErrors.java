package com.employeeapp.model;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ApiErrors {
    LocalDateTime localDateTime;
    HttpStatus status;
    int statusCode;
    String message;
    List<String> details;
}
