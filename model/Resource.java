package com.employeeapp.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Document
public class Resource {
    @Id
    private Integer resourceId;
    private String resourceName;
    @Field(targetType = FieldType.STRING)
    private ResourceType type; // electronic, vehicle, clothing, stationary, gifts
//    @Field(targetType = FieldType.STRING)
    private String grade; // manager, developer, tester, admin, networking
    private LocalDate issuedDate;
}
