package com.employeeapp.controllers;

import com.employeeapp.model.Employee;
import com.employeeapp.services.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/employee-api")
public class EmployeeController {
//    @Autowired
    private IEmployeeService employeeService;
    @Autowired
    public void setEmployeeService(IEmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping("/employees")
    void addEmployee(Employee employee){
        employeeService.addEmployee(employee);
    }
    @PutMapping("/employees")
    void updateEmployee(Employee employee){
        employeeService.updateEmployee(employee);
    }
    @DeleteMapping("/employees/id/{employeeId}")
    void deleteEmployee(@PathVariable("employeeId") int employeeId){
        employeeService.deleteEmployee(employeeId);
    }
    @GetMapping("/employees/id/{employeeId}")
    Employee getById(@PathVariable("employeeId") Integer employeeId){
        return employeeService.getById(employeeId);
    }
    @GetMapping("/employees")
    List<Employee> getAll(){
        return employeeService.getAll();
    }

    // derived
    @GetMapping("/employees/designation/{designation}")
    List<Employee> getByDesignation(@PathVariable("designation") String designation){
        return employeeService.getByDesignation(designation);
    };
    @GetMapping("/employees/department/{department}")
    List<Employee> getByDepartment(@PathVariable("department") String department){
        return employeeService.getByDepartment(department);
    }
    @GetMapping("/employees/address/city/{city}")
    List<Employee> geyByCity(@PathVariable("city") String city){
        return employeeService.getByCity(city);
    }

    // custom
    @GetMapping("/employees/address/state/{state}")
    List<Employee> getByState(@PathVariable("state") String state) {
        return employeeService.getByState(state);
    }
    @GetMapping("/employees/resources/{name}")
    List<Employee> getByResourceName(@PathVariable("name") String resourceName) {
        return employeeService.geyByResourceName(resourceName);
    }
    @GetMapping("/employees/resources/type/{type}")
    List<Employee> getByType(@PathVariable("type") String type) {
        return employeeService.getByType(type);
    }
    @GetMapping("/employees/resources/grade/{grade}")
    List<Employee> getByGrade(@PathVariable("grade") String grade) {
        return employeeService.getByGrade(grade);
    }

    @GetMapping("/employees/hobbies/{hobby}")
    List<Employee> getByHobbies(@PathVariable("hobby") String hobby) {
        return employeeService.getByHobbies(hobby);
    }
    List<Employee> getByGradeAndCity(String grade, String city) {
        return employeeService.getByGradeAndCity(grade, city);
    }
    List<Employee> getByDesignationAndType(String designation, String type) {
        return employeeService.getByDesignationAndType(designation, type);
    }
}
