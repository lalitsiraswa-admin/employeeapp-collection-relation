package com.employeeapp.controllers;

import com.employeeapp.model.Resource;
import com.employeeapp.services.IResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/resource-api")
public class ResourceController {
    private IResourceService resourceService;
    @Autowired
    public void setResourceService(IResourceService resourceService) {
        this.resourceService = resourceService;
    }
    @GetMapping("/resources")
    void addResource(Resource resource){
        resourceService.addResource(resource);
    }
}
