package com.employeeapp.services;

import com.employeeapp.model.Resource;
import com.employeeapp.repository.IResourceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResourseServiceImpl implements IResourceService{
    @Autowired
    private IResourceRepository resourceRepository;
    @Override
    public void addResource(Resource resource) {
        resourceRepository.insert(resource);
    }
}
