package com.employeeapp.services;

import com.employeeapp.exceptions.EmployeeNotFoundException;
import com.employeeapp.exceptions.IdNotFoundException;
import com.employeeapp.exceptions.ResourceNotFoundException;
import com.employeeapp.model.Employee;
import com.employeeapp.repository.IEmployeeRepository;
import com.mongodb.lang.NonNullApi;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements IEmployeeService{
    @Autowired
    private IEmployeeRepository employeeRepository;
    @Override
    public void addEmployee(Employee employee) {
        employeeRepository.insert(employee);
    }

    @Override
    public void updateEmployee(Employee employee) {
        employeeRepository.save(employee);
    }

    @Override
    public void deleteEmployee(int employeeId) {
        employeeRepository.deleteById(employeeId);
    }

    @Override
    public Employee getById(Integer employeeId) throws IdNotFoundException {
//        return employeeRepository.findById(employeeId).orElse(null);
        Optional<Employee> empFromDB = employeeRepository.findById(employeeId);
        if (empFromDB.isEmpty()) {
            return null;
        }
        return empFromDB.get();
    }

    @Override
    public List<Employee> getAll() {
        Sort sort = Sort.by(Sort.Direction.ASC, "name");
        return employeeRepository.findAll(sort);
    }

    @Override
    public List<Employee> getByDesignation(String designation) throws EmployeeNotFoundException{
        List<Employee> employees = employeeRepository.findByDesignation(designation, Sort.by("name"));
        if(employees.isEmpty())
            throw new EmployeeNotFoundException("Employee with this designation not found!");
        return employees;
    }

    @Override
    public List<Employee> getByDepartment(String department) throws EmployeeNotFoundException{
        List<Employee> employees = employeeRepository.findByDepartment(department);
        if(employees.isEmpty())
            throw new EmployeeNotFoundException("Employee in this department not found!");
        return employees;
    }

    @Override
    public List<Employee> getByCity(String city) throws EmployeeNotFoundException{
        List<Employee> employees = employeeRepository.findByAddressCity(city);
        if(employees.isEmpty())
            throw new EmployeeNotFoundException("Employee from this city not found!");
        return employees;
    }

    @Override
    public List<Employee> getByState(String state) throws EmployeeNotFoundException{
        List<Employee> employees = employeeRepository.findByState(state);
        if(employees.isEmpty())
            throw new EmployeeNotFoundException("Employee from this state not found!");
        return employees;
    }

    @Override
    public List<Employee> geyByResourceName(String resourceName) throws EmployeeNotFoundException {
        List<Employee> employees = employeeRepository.findByResourceName(resourceName);
        if(employees.isEmpty())
            throw new EmployeeNotFoundException("Employee with this resource not found!");
        return employees;
    }

    @Override
    public List<Employee> getByType(String type) throws ResourceNotFoundException{
        List<Employee> employees = employeeRepository.findByType(type);
        if(employees.isEmpty())
            throw new ResourceNotFoundException("Employee with this resource type not found!");
        return employees;
    }

    @Override
    public List<Employee> getByGrade(String grade) throws ResourceNotFoundException{
        List<Employee> employees = employeeRepository.findByGrade(grade);
        if(employees.isEmpty())
            throw new ResourceNotFoundException("Employee with this grade not found!");
        return employees;
    }

    @Override
    public List<Employee> getByGradeAndCity(String grade, String city) throws EmployeeNotFoundException{
        List<Employee> employees = employeeRepository.findByGradeAndCity(grade, city);
        if(employees.isEmpty())
            throw new ResourceNotFoundException("Employee with this grade and city not found!");
        return employees;
    }

    @Override
    public List<Employee> getByHobbies(String hobby) throws EmployeeNotFoundException{
        List<Employee> employees = employeeRepository.findByHobbies(hobby);
        if(employees.isEmpty())
            throw new ResourceNotFoundException("Employee with this hobby not found!");
        return employees;
    }

    @Override
    public List<Employee> getByDesignationAndType(String designation, String type) throws EmployeeNotFoundException{
        List<Employee> employees = employeeRepository.findByDesignationAndType(designation, type);
        if(employees.isEmpty())
            throw new ResourceNotFoundException("Employee with this resource grade not found!");
        return employees;
    }
}
