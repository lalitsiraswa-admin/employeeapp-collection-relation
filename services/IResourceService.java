package com.employeeapp.services;

import com.employeeapp.model.Resource;

public interface IResourceService {
    void addResource(Resource resource);
}
