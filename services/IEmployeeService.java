package com.employeeapp.services;

import com.employeeapp.exceptions.EmployeeNotFoundException;
import com.employeeapp.exceptions.IdNotFoundException;
import com.employeeapp.exceptions.ResourceNotFoundException;
import com.employeeapp.model.Employee;

import java.util.List;

public interface IEmployeeService {
    void addEmployee(Employee employee);
    void updateEmployee(Employee employee);
    void deleteEmployee(int employeeId);
    Employee getById(Integer employeeId) throws IdNotFoundException;
    List<Employee> getAll();

    // derived
    List<Employee> getByDesignation(String designation) throws EmployeeNotFoundException;
    List<Employee> getByDepartment(String department) throws EmployeeNotFoundException;
    List<Employee> getByCity(String city) throws EmployeeNotFoundException;

    // custom
    List<Employee> getByState(String state) throws EmployeeNotFoundException;
    List<Employee> geyByResourceName(String resourceName) throws EmployeeNotFoundException;
    List<Employee> getByType(String type) throws ResourceNotFoundException;
    List<Employee> getByGrade(String grade) throws ResourceNotFoundException;

    List<Employee> getByGradeAndCity(String grade, String city) throws EmployeeNotFoundException;
    List<Employee> getByHobbies(String hobby) throws EmployeeNotFoundException;
    List<Employee> getByDesignationAndType(String designation, String type) throws EmployeeNotFoundException;
}
